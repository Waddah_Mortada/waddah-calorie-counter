-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: calorie-counter
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `diaries`
--

DROP TABLE IF EXISTS `diaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `day` date NOT NULL,
  `food_id` int(10) unsigned NOT NULL,
  `grams` double(8,2) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `diaries_food_id_index` (`food_id`),
  CONSTRAINT `diaries_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diaries`
--

LOCK TABLES `diaries` WRITE;
/*!40000 ALTER TABLE `diaries` DISABLE KEYS */;
INSERT INTO `diaries` VALUES (1,'2016-07-01',1,50.00,'2016-07-05 08:19:49','2016-07-05 08:19:49'),(2,'2016-07-01',2,30.00,'2016-07-05 09:07:38','2016-07-05 09:07:38'),(3,'2016-07-08',3,500.00,'2016-07-05 10:09:14','2016-07-05 15:07:48'),(4,'2016-07-21',2,100.00,'2016-07-05 10:09:41','2016-07-05 10:09:41'),(5,'2016-07-11',1,100.00,'2016-07-05 10:09:59','2016-07-05 10:09:59'),(6,'2016-06-11',1,50.00,'2016-07-05 13:27:25','2016-07-05 13:27:25'),(7,'2016-06-11',3,20.00,'2016-07-05 13:27:32','2016-07-05 13:27:32'),(8,'2016-06-21',3,500.00,'2016-07-05 13:33:50','2016-07-05 13:33:50'),(9,'2016-08-21',2,550.00,'2016-07-05 13:34:21','2016-07-05 13:34:21'),(10,'2016-08-21',1,30.00,'2016-07-05 13:34:30','2016-07-05 13:34:30'),(11,'2016-08-25',1,50.00,'2016-07-05 13:34:53','2016-07-05 13:34:53'),(12,'2016-05-01',1,50.00,'2016-07-05 14:25:32','2016-07-05 14:25:32'),(13,'2016-05-01',1,50.00,'2016-07-05 14:25:32','2016-07-05 14:25:32'),(14,'2016-05-07',1,30.00,'2016-07-05 14:26:10','2016-07-05 14:26:10'),(15,'2016-05-07',1,30.00,'2016-07-05 14:26:10','2016-07-05 14:26:10'),(16,'2016-04-07',1,100.00,'2016-07-05 14:30:25','2016-07-05 14:30:25'),(17,'2016-05-18',3,20.00,'2016-07-05 14:31:16','2016-07-05 14:31:16'),(19,'2016-04-19',2,30.00,'2016-07-05 14:31:55','2016-07-05 14:31:55'),(22,'2016-07-17',2,30.00,'2016-07-05 15:15:49','2016-07-05 15:15:49'),(23,'2016-07-27',11,25.50,'2016-07-05 15:27:16','2016-07-05 15:27:16');
/*!40000 ALTER TABLE `diaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foods`
--

DROP TABLE IF EXISTS `foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `protein` double(8,2) DEFAULT NULL,
  `carbohydrate` double(8,2) DEFAULT NULL,
  `fat` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foods_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foods`
--

LOCK TABLES `foods` WRITE;
/*!40000 ALTER TABLE `foods` DISABLE KEYS */;
INSERT INTO `foods` VALUES (1,'Apple Pie',30.00,60.00,10.00,'2016-07-01 14:56:20','2016-07-04 14:53:02'),(2,'BEEF AND VEGETABLE',30.00,20.00,40.00,'2016-07-01 14:57:42','2016-07-04 14:53:26'),(3,'CHEESEBURGER',35.00,25.00,40.00,'2016-07-01 14:58:27','2016-07-04 14:54:27'),(11,'Pizza',20.00,30.00,50.00,'2016-07-05 15:26:49','2016-07-05 15:26:49');
/*!40000 ALTER TABLE `foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goals`
--

DROP TABLE IF EXISTS `goals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `calories_number` double(8,2) DEFAULT NULL,
  `perecentage` double(8,2) DEFAULT NULL,
  `calories` double(8,2) DEFAULT NULL,
  `grammes` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `goals_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goals`
--

LOCK TABLES `goals` WRITE;
/*!40000 ALTER TABLE `goals` DISABLE KEYS */;
INSERT INTO `goals` VALUES (1,'Calories Target',550.00,NULL,NULL,NULL,NULL,'2016-07-01 14:07:26'),(2,'Protein',NULL,50.00,275.00,68.75,NULL,'2016-07-01 14:07:26'),(3,'Carbohydrate',NULL,25.00,137.50,34.38,NULL,'2016-07-01 14:07:26'),(4,'Fat',NULL,25.00,137.50,15.28,NULL,'2016-07-01 14:07:26');
/*!40000 ALTER TABLE `goals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_07_01_101416_create_goals_table',1),('2016_07_01_142008_create_foods_table',2),('2016_07_04_083349_create_days_table',3),('2016_07_04_084253_create_diarys_table',4),('2016_07_04_141143_create_diaries_table',5),('2016_07_04_141804_create_diaries_table',6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-05 16:36:28
