<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Goal;

class Diary extends Model
{
    protected $fillable = ['day', 'food_id', 'grams'];

    public static function getCalories($grams, $pro, $carb, $fat)
    {
    	//protein
    	$pro = $pro/100;
    	$calPro = $grams * $pro;
    	$calPro = $calPro * 4;

    	//Carb
		$carb = $carb/100;
    	$calCarb = $grams * $carb;
    	$calCarb = $calCarb * 4;

    	//Fat
		$fat = $fat/100;
    	$calFat = $grams * $fat;
    	$calFat = $calFat * 9;

    	/*$thisFoodCal = $calPro + $calCarb + $calFat;
    	dd($thisFoodCal);*/

    	return $calPro + $calCarb + $calFat;
    }

    public static function getConsumed($date)
    {
        $date = date("Y-m-d", strtotime($date));
        try{
            $records = Diary::where('day', '=', $date)
                    ->get();
            $thisDateCal = 0;
            foreach ($records as $record) {
                $diary_id = $record->food_id;
                $var = Food::find($diary_id);
                $grams = $record->grams;
                $pro = $var['protein'];
                $carb = $var['carbohydrate'];
                $fat = $var['fat'];
                $thisFoodCal = Diary::getCalories($grams, $pro, $carb, $fat);
                $thisDateCal = $thisDateCal + $thisFoodCal;
            }
            return $thisDateCal;
        }
        catch(Exception $e) {
        }
        return null;
    }

    public static function getColor($consumed)
    {
    	$target = Goal::find(1);
    	$target = $target->calories_number;
    	if ($consumed > $target) {
    		$color = 'red';
    	}else{
    		$color = 'green';
    	}
    	return $color;
    }
}
