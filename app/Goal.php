<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Goal extends Model
{
	public static function updatePercentages($id, $perecentage, $calories, $grammes)
	{

        $record = Goal::find($id);
        $record->perecentage = $perecentage;
        $record->calories = $calories;
        $record->grammes = $grammes;
        //dd($record);
        $record->update();
	}

    public static function getGrammes($request)
    {
    	$calories_target = $request->calories;

    	$record = Goal::find(1);
        $record->calories_number = $calories_target;
        $record->update();

        $perecentages = $request->perecentage;

        $count = 0;
        foreach ($perecentages as $perecentage) {
            $count++;
            switch ($count) {
                case '1':
                    $perecentage = $perecentage; //protein
                    $equivalent = 4;
                    $id = 2;
                    break;

                case '2':
                    $perecentage = $perecentage; //carbohydrate
                    $equivalent = 4;
                    $id = 3;
                    break;

                case '3':
                    $perecentage = $perecentage; //fat
                    $equivalent = 9;
                    $id = 4;
                    break;
            }

        	$cal = $calories_target/100; //calories for every percentage

        	$calories = $cal * $perecentage; //calories for mac per day

        	$grammes = $calories/$equivalent; //grammes for mac per day

        	Goal::updatePercentages($id, $perecentage, $calories, $grammes);
        }
    }
}
