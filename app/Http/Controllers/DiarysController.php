<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Food;
use App\Goal;
use App\Diary;
use DB;

class DiarysController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nextmonth = 0;
        $currentmonth = 0;
        $Current_Month_Disply = null;
        $currentDate = null;
        $addMonth = null;
        $subMonth = null;
        $addMonth = $request->addMonth;
        $subMonth = $request->subMonth;
        //dd($request);
        if (($addMonth == null) & ($subMonth == null)) {
            $currentDate = date("d-m-Y");

            list($day,$month,$year) = explode("-",$currentDate);

            $monthsDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $month;

            $day = min(1,date("t",strtotime($year."-".$month."-01"))); 

            $date = $day."-".$month."-".$year;

            $currentmonth = date("d-m-Y", strtotime($date));

            $dw = date('N', strtotime($date));

            $next_date = $currentmonth;
            $monthDates[] = $currentmonth;
            $month = date("m-Y", strtotime($currentmonth));
            $day = date("d", strtotime($next_date));
            $days[] = $day;
            $Current_Month_Disply = $currentmonth;
            $current = true;
        }elseif ($addMonth == 1) {
            $thisDate = $request->currentDate;

            list($day,$month,$year) = explode("-",$thisDate);

            $month++;

            $monthsDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            //dd($monthsDays);

            $day = min(1,date("t",strtotime($year."-".$month."-01"))); 

            $newDate = $day."-".$month."-".$year;

            $nextmonth = date("d-m-Y", strtotime($newDate));

            $dw = date('N', strtotime($newDate));
            //dd($dw);
            $next_date = $nextmonth;
            $monthDates[] = $nextmonth;
            $month = date("m-Y", strtotime($nextmonth));
            $day = date("d", strtotime($next_date));
            $days[] = $day;
            $Current_Month_Disply = $nextmonth;
            $current = true;
        }elseif ($subMonth == 0) {
            $thisDate = $request->currentDate;
            list($day,$month,$year) = explode("-",$thisDate);

            $month--;

            $monthsDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            //dd($monthsDays);

            $day = min(1,date("t",strtotime($year."-".$month."-01"))); 

            $newDate = $day."-".$month."-".$year;

            $nextmonth = date("d-m-Y", strtotime($newDate));

            $dw = date('N', strtotime($newDate));
            //dd($dw);
            $next_date = $nextmonth;
            $monthDates[] = $nextmonth;
            $month = date("m-Y", strtotime($nextmonth));
            $day = date("d", strtotime($next_date));
            $days[] = $day;
            $Current_Month_Disply = $nextmonth;
            $current = false;
        }
        //dd($currentDate, $currentmonth, $monthsDays, $dw, $next_date, $monthDates, $days, $emptyCells);
        //$next_date = date('d-m-Y', strtotime($date .' +1 day'));

        for ($i=1; $i < $monthsDays; $i++) { 

            $next_date = date('d-m-Y', strtotime($next_date .' +1 day'));
            $monthDates[] = $next_date;
            $month = date("m-Y", strtotime($next_date));
            $day = date("d", strtotime($next_date));
            $days[] = $day;
        }

        $x = 0;
        //$dw -= 1;
        $y = $dw - 1;
        $emptyCells = array ();
        $empty = false;
        if ($dw > 1) {
            do {
                $emptyCells[] = "";
                $x++;
            } while ( $x < $y);
            $empty = true;
        }

        $count = 0;
        $rows = array ();
        $row1 = array ();
        $row2 = array ();
        $row3 = array ();
        $row4 = array ();
        $row5 = array ();
        $row6 = array ();
        $a = 0;
        $b = 0;
        $c = 0;
        //$x++;
        //dd($currentDate, $currentmonth, $nextmonth, $monthsDays, $dw, $next_date, $monthDates, $days, $emptyCells, $Current_Month_Disply);
        $daysConsumed = 0;
        $totalConsumed = 0;
        //6 rows
        if ($current == true) {
            for ($i=1; $i < 7; $i++) { 
                //7 cells
                for ($j=0; $j < 7; $j++) { 
                    
                    if ($empty == true) {
                        if ($a < $y) {
                            $rows[$i][] = "<td align=center>".$emptyCells[$a]."</td>";
                            $a++;
                        }else{
                            $count = 7 - $x;
                            if ($b < $count) {
                                //$rows[$i][] = "<a href='diary/".$monthDates[$b]."'>".$days[$b]."</a>";
                                $valLink = "<a href='diary/".$monthDates[$b]."'>".$days[$b]."</a>";
                                $thisDateConsumed = Diary::getConsumed($monthDates[$b]);
                                if ($thisDateConsumed != null) {
                                    $daysConsumed++;
                                    $totalConsumed += $thisDateConsumed;
                                    $color = Diary::getColor($thisDateConsumed);
                                    $valStyle = "<td align=center bgcolor=".$color.">".$valLink."</td>";
                                }else{ 
                                    $valStyle = "<td align=center>".$valLink."</td>";
                                }
                                $rows[$i][] = $valStyle;
                                $b++;
                            }
                        }
                        $c++;
                        if ($c == 7) {
                            $empty = false;
                        }
                    }else{
                        if ($count < $monthsDays) {
                            $valLink = "<a href='diary/".$monthDates[$count]."'>".$days[$count]."</a>";
                            $thisDateConsumed = Diary::getConsumed($monthDates[$count]);
                            if ($thisDateConsumed != null) {
                                $daysConsumed++;
                                $totalConsumed += $thisDateConsumed;
                                $color = Diary::getColor($thisDateConsumed);
                                $valStyle = "<td align=center bgcolor=".$color.">".$valLink."</td>";
                            }else{ 
                                $valStyle = "<td align=center>".$valLink."</td>";
                            }
                            $rows[$i][] = $valStyle;
                            //$rows[$i][] = "<a href='diary/".$monthDates[$count]."'>".$days[$count]."</a>";
                            $count++;
                        }else{
                            $rows[$i][] = "<td align=center> </td>";
                        }
                    }
                }
            }
        }elseif ($current == false) {
            for ($i=1; $i < 7; $i++) { 
                //7 cells
                for ($j=0; $j < 7; $j++) { 
                    
                    if ($empty == true) {
                        if ($a < $y) {
                            $rows[$i][] = "<td align=center>".$emptyCells[$a]."</td>";
                            $a++;
                        }else{
                            $count = 7 - $x;
                            if ($b < $count) {
                                //$rows[$i][] = "<a href='diary/".$monthDates[$b]."'>".$days[$b]."</a>";
                                $valLink = "<a href='".$monthDates[$b]."'>".$days[$b]."</a>";
                                $thisDateConsumed = Diary::getConsumed($monthDates[$b]);
                                if ($thisDateConsumed != null) {
                                    $daysConsumed++;
                                    $totalConsumed += $thisDateConsumed;
                                    $color = Diary::getColor($thisDateConsumed);
                                    $valStyle = "<td align=center bgcolor=".$color.">".$valLink."</td>";
                                }else{ 
                                    $valStyle = "<td align=center>".$valLink."</td>";
                                }
                                $rows[$i][] = $valStyle;
                                $b++;
                            }
                        }
                        $c++;
                        if ($c == 7) {
                            $empty = false;
                        }
                    }else{
                        if ($count < $monthsDays) {
                            $valLink = "<a href='".$monthDates[$count]."'>".$days[$count]."</a>";
                            $thisDateConsumed = Diary::getConsumed($monthDates[$count]);
                            if ($thisDateConsumed != null) {
                                $daysConsumed++;
                                $totalConsumed += $thisDateConsumed;
                                $color = Diary::getColor($thisDateConsumed);
                                $valStyle = "<td align=center bgcolor=".$color.">".$valLink."</td>";
                            }else{ 
                                $valStyle = "<td align=center>".$valLink."</td>";
                            }
                            $rows[$i][] = $valStyle;
                            //$rows[$i][] = "<a href='diary/".$monthDates[$count]."'>".$days[$count]."</a>";
                            $count++;
                        }else{
                            $rows[$i][] = "<td align=center> </td>";
                        }
                    }
                }
            }
        }
        
        //dd($rows, $a, $b, $x, $dw);
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            switch ($counter) {
                case '1':
                    for ($i=0; $i < 7; $i++) { 
                        $row1[] = $row[$i];
                    }
                    break;

                case '2':
                    for ($i=0; $i < 7; $i++) { 
                        $row2[] = $row[$i];
                    }
                    break;

                case '3':
                    for ($i=0; $i < 7; $i++) { 
                        $row3[] = $row[$i];
                    }
                    break;

                case '4':
                    for ($i=0; $i < 7; $i++) { 
                        $row4[] = $row[$i];
                    }
                    break;

                case '5':
                    for ($i=0; $i < 7; $i++) { 
                        $row5[] = $row[$i];
                    }
                    break;

                case '6':
                    for ($i=0; $i < 7; $i++) { 
                        $row6[] = $row[$i];
                    }
                    break;
            }
        }

        $daysConsumed;
        $totalConsumed;
        $target = Goal::find(1);
        $target = $target->calories_number;
        $effectiveTarget = $target * $daysConsumed;
        $difference = $totalConsumed - $effectiveTarget;
        if ($difference > 0) {
            $color = 'red';
        }else{
            $color = 'green';
        }

        $dispalyMonth = date("F Y", strtotime($Current_Month_Disply));

        //dd($row1, $row2, $row3, $row4, $row5, $row6);

        //dd($daysConsumed, $totalConsumed);

        //dd($currentDate, $nextmonth, $monthsDays, $dw, $next_date, $monthDates, $days, $emptyCells);

        return view('diary.index', compact('dispalyMonth', 'Current_Month_Disply', 'currentDate', 'days', 'monthsDays', 'totalConsumed', 'effectiveTarget', 'difference', 'color', 'row1', 'row2', 'row3', 'row4', 'row5', 'row6'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request = $request->all();
        $id = $request['name'];
        $grams = $request['grams'];
        $date = $request['date'];
        $date = date("Y-m-d", strtotime($date));
        $foods = ['day' => $date, 'food_id' => $id, 'grams' => $grams];
        $food = new Diary($foods);
        $food->save();
        $url = 'http://calorie-counter.app/diary/'.$request['date'];

        return \Redirect::to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($date)
    {
        $diaries = array ();
        $date = date("Y-m-d", strtotime($date));
        try{
            $records = Diary::where('day', '=', $date)
                    ->get();
            $TotalCal = 0;
            foreach ($records as $record) {
                $diary_id = $record->food_id;
                $var = Food::find($diary_id);
                $food = ['name' => $var['name'], 'grams' => $record->grams, 'id' => $record->id];
                $grams = $record->grams;
                $pro = $var['protein'];
                $carb = $var['carbohydrate'];
                $fat = $var['fat'];
                $thisFoodCal = Diary::getCalories($grams, $pro, $carb, $fat);
                $TotalCal += $thisFoodCal;
                $diaries[] = $food;
            }
        }
        catch(Exception $e) {
        }

        $date = date("d-m-Y", strtotime($date));
        $Current_Month_Disply = $date;
        $foods = Food::get();


        return View('diary.show', compact('diaries', 'foods', 'date', 'TotalCal', 'Current_Month_Disply'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diary = Diary::find($id);
        $foods = Food::all();

        return View('diary.edit', compact('foods', 'diary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->except(['_method','_token']);

        $request = $request->all();
        $food_id = $request['name'];
        $grams = $request['grams'];
        $date = $request['date'];

        $diary = Diary::find($id);
        $diary->food_id = $food_id;
        $diary->grams = $grams;
        $diary->update();
        $url = 'http://calorie-counter.app/diary/'.$date;

        return \Redirect::to($url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diary = Diary::find($id);
        $date = $diary->day;
        $diary->delete();

        $url = 'http://calorie-counter.app/diary/'.$date;

        return \Redirect::to($url);
    }
}
