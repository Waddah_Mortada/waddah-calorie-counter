<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Food;

class FoodsController extends Controller
{
    public function index()
    {
        $foods = Food::all();

        return view('foods.index', compact('foods'));
    }

    public function create()
    {
        return view('foods.new');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $food = new Food($request->all());
        $food->save();

        return \Redirect::to('http://calorie-counter.app/foods');
    }

    public function show($id)
    {
        $food = Food::find($id);

        return View('foods.show', compact('food'));
    }

    public function edit($id)
    {
        $food = Food::find($id);

        return View('foods.edit', compact('food'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['_method','_token']);
        Food::whereId($id)->update($data);

        return \Redirect::to('http://calorie-counter.app/foods');
    }

    public function destroy($id)
    {
        $food = Food::find($id);
        $food->delete();

        return \Redirect::to('http://calorie-counter.app/foods');
    }
}
