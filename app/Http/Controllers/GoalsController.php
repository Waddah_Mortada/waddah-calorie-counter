<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Goal;

use DB;

class GoalsController extends Controller
{
    public function home()
    {
        return view('welcome');
    }

    public function index()
    {
        $goals = Goal::all();

        return view('goals.index', compact('goals'));
    }

    public function create()
    {
        $t_cal = Goal::find(1);
        $t_cal = $t_cal->calories_number;


        $p = Goal::find(2);
        $p = $p->perecentage;


        $c = Goal::find(3);
        $c = $c->perecentage;


        $f = Goal::find(4);
        $f = $f->perecentage;

        return view('goals.new', compact('t_cal', 'p', 'c', 'f'));
    }

    public function store(Request $request)
    {
        Goal::getGrammes($request);

        return \Redirect::to('http://calorie-counter.app/goals');
    }

    public function edit()
    {
        $t_cal = Goal::find(1);
        $t_cal = $t_cal->calories_number;


        $p = Goal::find(2);
        $p = $p->perecentage;


        $c = Goal::find(3);
        $c = $c->perecentage;


        $f = Goal::find(4);
        $f = $f->perecentage;

        return view('goals.edit', compact('t_cal', 'p', 'c', 'f'));
    }
}
