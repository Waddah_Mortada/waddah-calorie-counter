<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'GoalsController@home');

Route::get('/goals', 'GoalsController@index');

Route::get('/goals/create', 'GoalsController@create');

Route::get('/goals/edit', 'GoalsController@edit');

Route::post('/goals/store', 'GoalsController@store');


Route::get('/foods', 'FoodsController@index');

Route::get('/foods/create', 'FoodsController@create');

Route::post('/foods/store', 'FoodsController@store');

Route::get('/foods/{food}', 'FoodsController@show');

Route::get('/foods/edit/{food}', 'FoodsController@edit');

Route::patch('/foods/update/{food}', 'FoodsController@update');

Route::delete('/foods/delete/{food}', 'FoodsController@destroy');


Route::get('/diary', 'DiarysController@index');

Route::post('/diary/', 'DiarysController@index');

Route::get('/diary/{date}', 'DiarysController@show');

Route::post('/diary/store', 'DiarysController@store');

Route::get('/diary/edit/{diary}', 'DiarysController@edit');

Route::patch('/diary/update/{diary}', 'DiarysController@update');

Route::delete('/diary/delete/{diary}', 'DiarysController@destroy');


