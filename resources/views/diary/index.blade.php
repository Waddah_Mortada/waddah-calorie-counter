@extends('layouts.master')

@section('content')


<h3 class="page-title">Diary</h3>
<div class="page-bar"></div>


<form method="POST" action="http://calorie-counter.app/diary/" accept-charset="UTF-8" role="form" class="form-horizontal">
	{{ csrf_field() }}

<div class="portlet box blue-hoki">
	<div class="portlet-title">
		<div class="caption">
			Diary
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-responsive">

		<TABLE BORDER=3 CELLSPACING=3 CELLPADDING=3> 
			<tr>
				<form method="POST" action="http://calorie-counter.app/diary" accept-charset="UTF-8" role="form" class="form-horizontal">
					{{ csrf_field() }}
					<input type="hidden" name="subMonth" value="0">
					<input type="hidden" name="currentDate" value="{{ $Current_Month_Disply }}">
					<td><input class="btn btn" type="submit" value="<"></td> 
				</form>
				<td COLSPAN="5" align=center><B>{{$dispalyMonth}}</B></td> 
				<form method="POST" action="http://calorie-counter.app/diary" accept-charset="UTF-8" role="form" class="form-horizontal">
					{{ csrf_field() }}
					<input type="hidden" name="addMonth" value="1">
					<input type="hidden" name="currentDate" value="{{ $Current_Month_Disply }}">
					<td><input class="btn btn" type="submit" value=">"></td> 
				</form>
			</tr>
			<tr> 
				<td COLSPAN="7" align=center>
					<I>
						Number of Calories Consumed: {{$totalConsumed}}
					</I>
				</td>
			</tr>
			<tr> 
				<td COLSPAN="7" align=center>
					<I>
					Effective Target Calories: {{$effectiveTarget}}
					</I>
				</td>
			</tr>
			<tr> 
				<td COLSPAN="7" align=center>
					<I>
					Difference: <span style="color:<?php echo $color;?>"> {{$difference}}</span>
					</I>
				</td>
			</tr>
			<tr> 
				<td align=center><b>&nbsp;Mon&nbsp;</b></td>
				<td align=center><b>&nbsp;Tue&nbsp;</b></td>
				<td align=center><b>&nbsp;Wed&nbsp;</b></td>
				<td align=center><b>&nbsp;Thu&nbsp;</b></td>
				<td align=center><b>&nbsp;&nbsp;Fri&nbsp;&nbsp;</b></td>
				<td align=center><b>&nbsp;Sat&nbsp;&nbsp;</b></td>
				<td align=center><b>&nbsp;Sun&nbsp;&nbsp;</b></td>
			</tr>
			<tr> 
				@foreach ($row1 as $row)
					<?php echo $row;?>
				@endforeach
			</tr>
			<tr> 
				@foreach ($row2 as $row)
					<?php echo $row;?>
				@endforeach
			</tr>
			<tr> 
				@foreach ($row3 as $row)
					<?php echo $row;?>
				@endforeach
			</tr>
			<tr> 
				@foreach ($row4 as $row)
					<?php echo $row;?>
				@endforeach
			</tr>
			<tr> 
				@foreach ($row5 as $row)
					<?php echo $row;?>
				@endforeach
			</tr>
			<tr> 
				@foreach ($row6 as $row)
					<?php echo $row;?>
				@endforeach
			</tr>
		</div>
	</div>
</div>

@endsection