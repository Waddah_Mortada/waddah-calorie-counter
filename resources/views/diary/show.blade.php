@extends('layouts.master')

@section('content')


<h3 class="page-title">My Diary ({{ $date }})</h3>
<div class="page-bar"></div>

<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-reorder"></i> New
		</div>
	</div>
	<div class="portlet-body form">
		<form method="POST" action="http://calorie-counter.app/diary/store" accept-charset="UTF-8" role="form" class="form-horizontal">
			{{ csrf_field() }}
			<div class="form-body">
				<div class="form-group ">
					<label class="col-md-3 control-label">Food<span class="required">*</span>:</label>
					<div class="col-md-5">
						<select class="form-control" name="name">
							<option value="" selected="selected">Select...</option>
							@foreach($foods as $food)
                            	@if ($food->id != null)
                            		<option value="{{ $food->id }}">{{ $food->name }}</option>
                            	@endif
                        	@endforeach
						</select>
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Grams<span class="required">*</span>:</label>
					<div class="col-md-5">
						<input class="form-control" name="grams" required="" type="text">
					</div>
				</div>
				<p>
					<em>* Required fields</em>
				</p>
			</div>
			<div class="form-actions">
				<div class="col-md-offset-3 col-md-9">
					<input type="hidden" name="date" value="{{ $date }}">
					<input type="hidden" name="currentDate" value="{{ $Current_Month_Disply }}">
					<input class="btn btn-primary" type="submit" value="Add +">
				</div>
			</div>
		</form>
	</div>
</div>


<div class="portlet box blue-hoki">
	<div class="portlet-title">
		<div class="caption">
			Diary ({{ $date }})
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-responsive">
			<table class="table dataTable table-striped table-hover table-bordered flip-content">
				<thead>
					<tr class="sort-header">
						<th class="text-center">Food</th>
						<th class="text-center">Grams (g)</th>
						<th class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($diaries as $diary)
					<tr>
						<td class="text-center"> {{ $diary['name'] }} </td>
						<td class="text-center">{{ $diary['grams'] }}</td>
						<td class="text-center">
							<a class="btn btn-xs blue" 
								href="http://calorie-counter.app/diary/edit/{{$diary['id']}}" 
								title="Edit record">
								<i class="fa fa-pencil"></i>
								Edit
							</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			<tr style=" border: 1px solid black;">
				<td class="text-center" style=" border: 1px solid black;"><b> Total Calories: </b></td>
				<td class="text-center" style=" border: 1px solid black;"> {{ $TotalCal }} </td>
			</tr>
		</div>
	</div>
</div>

@endsection