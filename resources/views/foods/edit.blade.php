@extends('layouts.master')

@section('content')

<h3 class="page-title">{{ $food->name }}</h3>
<div class="page-bar"></div>
<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-reorder"></i> Edit record
		</div>
		<div class="actions">
			<a href="#confirm_delete" class="red btn btn-warning" role="button" data-toggle="modal">
				<i class="fa fa-trash-o"></i> Delete
			</a>
		</div>
	</div>
	<div class="portlet-body form">
		<form method="POST" action="http://calorie-counter.app/foods/update/{{ $food->id }}" accept-charset="UTF-8" role="form" class="form-horizontal">
			{{ method_field('PATCH')}}
			{{ csrf_field() }}
			<div class="form-body">
				<div class="form-group ">
					<label class="col-md-3 control-label">Name<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="name" type="text" required="" value="{{ $food->name }}">	
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Protein (%)<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="protein" type="text" required="" value="{{ $food->protein }}">	
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Carbohydrate (%)<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="carbohydrate" type="text" required="" value="{{ $food->carbohydrate }}">	
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Fat (%)<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="fat" type="text" required="" value="{{ $food->fat }}">	
					</div>
				</div>
				<p>
					<em>* Required fields</em>
				</p>
				<p>
					<em><span style="color:red">Note: </span>The total percentage of all three macronutrients should be 100%.</em>
				</p>
			</div>

			<div class="form-actions">
				<div class="col-md-offset-3 col-md-9">
					<a class="btn btn-default" href="http://calorie-counter.app/foods/{{ $food->id }}">
						Cancel
					</a>
					<input class="btn btn-primary" type="submit" value="Save">
				</div>
			</div>
		</form>
		@if (count($errors))
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error}}</li>
				@endforeach
			</ul>
		@endif
	</div>	
</div>

<div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-exclamation-triangle"></i> Delete record?</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you would like to delete this type?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">
					<i class="fa fa-times"></i> Cancel
				</button>
				<form method="POST" action="http://calorie-counter.app/foods/delete/{{ $food->id }}" accept-charset="UTF-8" class="inline">
					{{ method_field('DELETE')}}
					{{ csrf_field() }}
					<button type="submit" class="red btn btn-warning">
						<i class="fa fa-trash-o"></i> Delete
					</button>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection