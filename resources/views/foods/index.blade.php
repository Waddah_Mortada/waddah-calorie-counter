@extends('layouts.master')

@section('content')


<h3 class="page-title">My Foods</h3>
<div class="page-bar"></div>


<div class="portlet box blue-hoki">
	<div class="portlet-title">
		<div class="caption">
			My Foods
		</div>
		<div class="actions">
			<a class="btn green" href="http://calorie-counter.app/foods/create">
				<i class="fa fa-plus"></i> Enter Foods Target
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-responsive">
			<table class="table dataTable table-striped table-hover table-bordered flip-content">
				<thead>
					<tr class="sort-header">
						<th>Name</th>
						<th class="text-center">Protein (%)</th>
						<th class="text-center">Carbohydrate (%)</th>
						<th class="text-center">Fat (%)</th>
						<th class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($foods as $food)
					<tr>
						<td> {{ $food->name }} </td>
						<td class="text-center">{{ $food->protein }}</td>
						<td class="text-center">{{ $food->carbohydrate }}</td>
						<td class="text-center">{{ $food->fat }}</td>
						<td class="text-center">
							<a class="btn btn-xs purple" 
								href="http://calorie-counter.app/foods/{{$food->id}}" 
								title="View record">
								<i class="fa fa-search"></i>
								View
							</a>
							&ensp;
							<a class="btn btn-xs blue" 
								href="http://calorie-counter.app/foods/edit/{{$food->id}}" 
								title="Edit record">
								<i class="fa fa-pencil"></i>
								Edit
							</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection