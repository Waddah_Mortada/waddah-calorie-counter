@extends('layouts.master')

@section('content')

<h3 class="page-title">New Food</h3>
<div class="page-bar"></div>
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-reorder"></i> New Food
		</div>
	</div>
	<div class="portlet-body form">
		<form method="POST" action="http://calorie-counter.app/foods/store" accept-charset="UTF-8" role="form" class="form-horizontal">
			{{ csrf_field() }}
			<div class="form-body">
				<div class="form-group ">
					<label class="col-md-3 control-label">Name of Food<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="name" required="" type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Protein (%)<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="protein" required="" type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Carbohydrate (%)<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="carbohydrate" required="" type="text">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Fat (%)<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="fat" required="" type="text">
					</div>
				</div>
				<p>
					<em>* Required fields</em>
				</p>
				<p>
					<em><span style="color:red">Note: </span>The total percentage of all three macronutrients should be 100%.</em>
				</p>
			</div>
			<div class="form-actions">
				<div class="col-md-offset-3 col-md-9">
					<a class="btn btn-default" href="http://calorie-counter.app/foods">Back</a>
					<input class="btn btn-primary" type="submit" value="Save">
				</div>
			</div>
		</form>
		@if (count($errors))
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error}}</li>
				@endforeach
			</ul>
		@endif
	</div>
</div>



@endsection