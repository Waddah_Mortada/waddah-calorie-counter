@extends('layouts.master')

@section('content')

<h3 class="page-title">{{$food->name}}</h3>
<div class="page-bar"></div>
<div class="portlet box purple">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-reorder"></i> View record
		</div>
		<div class="actions">
			<a class="btn blue" href="http://calorie-counter.app/foods/edit/{{ $food->id }}">
				<i class="fa fa-pencil"></i> Edit
			</a>
		</div>
	</div>
	<div class="portlet-body form">
		<form class="form-horizontal">
			<div class="form-body">
				<div class="form-group">
					<label class="col-md-3 control-label">Name:</label>
					<div class="col-md-9">
						<p class="form-control-static">{{$food->name}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Protein (%):</label>
					<div class="col-md-9">
						<p class="form-control-static">{{$food->protein}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Carbohydrate (%):</label>
					<div class="col-md-9">
						<p class="form-control-static">{{$food->carbohydrate}}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Fat (%):</label>
					<div class="col-md-9">
						<p class="form-control-static">{{$food->fat}}</p>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="col-md-offset-3 col-md-9">
					<a class="btn btn-default" href="http://calorie-counter.app/foods">
						Back
					</a>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection