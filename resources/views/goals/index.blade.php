@extends('layouts.master')

@section('content')


<h3 class="page-title">My Goals</h3>
<div class="page-bar"></div>


<div class="portlet box blue-hoki">
	<div class="portlet-title">
		<div class="caption">
			My Goals
		</div>
		<div class="actions">
			<a class="btn green" href="http://calorie-counter.app/goals/create">
				<i class="fa fa-plus"></i> Enter Goals Target
			</a>
			<a class="btn purple" href="http://calorie-counter.app/goals/edit">
				<i class="fa fa-pencil"></i> Modify Goals Target
			</a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-responsive">
			<table class="table dataTable table-striped table-hover table-bordered flip-content">
				<thead>
					<tr class="sort-header">
						<th></th>
						<th class="text-center">Target Number of Calories</th>
						{{--<th class="text-center">Perecentage per day</th>
						<th class="text-center">Calories per day</th>--}}
						<th class="text-center">Grammes per day</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($goals as $goal)
					<tr>
						<td><b> {{ $goal->name }} </b></td>
						<td class="text-center">
							@if ($goal->calories_number == null)
								-
							@endif
							{{ $goal->calories_number }}
						</td>
						{{--<td class="text-center">
							@if ($goal->perecentage == null)
								-
							@endif
							{{ $goal->perecentage }}
						</td>
						<td class="text-center">
							@if ($goal->calories == null)
								-
							@endif
							{{ $goal->calories }}
						</td>--}}
						<td class="text-center">
							@if ($goal->grammes == null)
								-
							@endif
							{{ $goal->grammes }}
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection