@extends('layouts.master')

@section('content')

<h3 class="page-title">Target Goals</h3>
<div class="page-bar"></div>
<div class="portlet box green">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-reorder"></i> Target Goals
		</div>
	</div>
	<div class="portlet-body form">
		<form method="POST" action="http://calorie-counter.app/goals/store" accept-charset="UTF-8" role="form" class="form-horizontal">
			{{ csrf_field() }}
			<div class="form-body">
				<div class="form-group ">
					<label class="col-md-3 control-label">Calories<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="calories" required="" type="text" value="{{$t_cal}}">
						<span id="helpBlock2" class="help-block">Target number of calories in a day.
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Protein %<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="perecentage[protein]" required="" type="text" value="{{$p}}">
						<span id="helpBlock2" class="help-block">The perecentage of protein in a day.
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Carbohydrate %<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="perecentage[carbohydrate]" required="" type="text" value="{{$c}}">
						<span id="helpBlock2" class="help-block">The perecentage of carbohydrate in a day.
					</div>
				</div>
				<div class="form-group ">
					<label class="col-md-3 control-label">Fat %<span class="required">*</span>:</label>
					<div class="col-md-9">
						<input class="form-control" name="perecentage[fat]" required="" type="text" value="{{$f}}">
						<span id="helpBlock2" class="help-block">The perecentage of fat in a day.
					</div>
				</div>
				<p>
					<em>* Required fields</em>
				</p>
				<p>
					<em><span style="color:red">Note: </span>The total percentage of all three macronutrients should be 100%.</em>
				</p>
			</div>
			<div class="form-actions">
				<div class="col-md-offset-3 col-md-9">
					<a class="btn btn-default" href="http://calorie-counter.app/goals">Back</a>
					<input class="btn btn-primary" type="submit" value="Save">
				</div>
			</div>
		</form>
		@if (count($errors))
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error}}</li>
				@endforeach
			</ul>
		@endif
	</div>
</div>



@endsection