<!doctype html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!-- BEGIN HEAD -->
    <head>
        <title>Calorie Counter :: Charabanc powered by Fifteen Digital</title><meta name="description" content="Series" /><meta name="keywords" content="Series, Charabanc powered by Fifteen Digital" />
        <meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link media="all" type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all">

<link media="all" type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/bootstrap/css/bootstrap.min.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/uniform/css/uniform.default.css">

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/css/components.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/css/plugins.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/admin/layout/css/layout.css">

<link id="style_color" media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/admin/layout/css/themes/darkblue.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/admin/layout/css/custom.css">

<!-- END THEME STYLES -->

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/css/general.css">


<link rel="shortcut icon" href="/favicon.ico"/>     
    <link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/select2/select2.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css">

<link media="all" type="text/css" rel="stylesheet" href="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/bootstrap-datepicker/css/datepicker.css">


    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-header-fixed page-quick-sidebar-over-content page-style-square">

        <!-- BEGIN HEADER -->
        <!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="http://lessons.myfifteen.co.uk">
                <img src="http://lessons.myfifteen.co.uk/assets/images/cube_small.png" alt="logo" />
                <span class="app-title">Connect</span>
            </a>
            &nbsp;
            <small>powered by <a href="http://fifteendigital.co.uk" target="_blank">Fifteen Digital</a></small>
            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span class="username username-hide-on-mobile">
                    Waddah Mortada </span>
                    <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="http://lessons.myfifteen.co.uk/users/12/edit">
                            <i class="icon-user"></i> My Profile </a>
                        </li>
                                                <li>
                            <a href="http://lessons.myfifteen.co.uk/logout">
                            <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="http://lessons.myfifteen.co.uk/logout" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                    </a>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->


                </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
        <!-- END HEADER -->

        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- BEGIN SIDEBAR MENU -->
<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        @include('layouts.sidebar')

                        <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    

                @yield('content')
                
                                
                <div class="clearfix"></div>
                            
            </div>
        </div>

                    
                </div>  
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
         &copy; 2016 Fifteen Digital
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/respond.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/excanvas.min.js"></script>
 
<![endif]-->
<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/jquery.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/jquery-migrate.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/jquery.blockui.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/jquery.cokie.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/uniform/jquery.uniform.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>


<script src="http://lessons.myfifteen.co.uk/assets/js/util.js"></script>
<script src="http://lessons.myfifteen.co.uk/assets/js/global.js"></script>
<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/scripts/metronic.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/admin/layout/scripts/layout.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/admin/layout/scripts/quick-sidebar.js"></script>


<script src="http://lessons.myfifteen.co.uk/assets/js/logo.js"></script>


<script>
$(function() {
    Metronic.init();
    Layout.init();
    QuickSidebar.init();
});
</script>
<!-- END CORE PLUGINS -->

<script>
var url_root = "http://lessons.myfifteen.co.uk";
</script>
<script type="text/javascript">
    var $buoop = {vs:{i:10,f:20,o:15,s:6}};
    $buoop.ol = window.onload;
    window.onload=function(){
        try {if ($buoop.ol) $buoop.ol();}catch (e) {}
        var e = document.createElement("script");
        e.setAttribute("type", "text/javascript");
        e.setAttribute("src", "//browser-update.org/update.js");
        document.body.appendChild(e);
    }
</script>
        <!-- END JAVASCRIPTS -->


<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script>
    $(function() {
        $('.datepicker').datepicker({ format:'yyyy/mm/dd'});
    });
    </script>
    
        
    </body>
    <!-- END BODY -->

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/select2/select2.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins//datatables/media/js/jquery.dataTables.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/vendor/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="http://lessons.myfifteen.co.uk/assets/js/data_table.js"></script>

<script>
$(function() {       
    $('.sort-header th').click(function() {
        var url = $(this).data('url');
        if (url != undefined) {
            window.location = url;
        }
    });
    $('select[name=records]').change(function() {
        $(this).parents('form').submit();
    });
});
</script>;

        
    </body>
    <!-- END BODY -->
    
</html>
