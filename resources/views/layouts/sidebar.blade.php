<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <li class="sidebar-toggler-wrapper">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
    </li>
    <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
    <li>
        &nbsp;
    </li>
        
    <li class="start">
        <a href="/">
            <i class="fa fa-home"></i>
            <span class="title">
                Home
            </span>
        </a>
    </li>           
    <li>
        <a href="/goals">
            <i class="fa fa-star"></i>
            <span class="title">
                My Goals
            </span>
        </a>
    </li>  
    <li>
        <a href="/foods">
            <i class="fa fa-star"></i>
            <span class="title">
                My Foods
            </span>
        </a>
    </li>  
    <li>
        <a href="/diary">
            <i class="fa fa-star"></i>
            <span class="title">
                Diary
            </span>
        </a>
    </li>  
</ul> 